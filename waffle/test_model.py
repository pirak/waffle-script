# -*- coding: utf-8 -*-

# Copyright (C) 2020 pirak
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

import string


class Data:
    """
    https://github.com/AnkhHeart/Streamlabs-Chatbot-Python-Boilerplate/wiki/Data-Object
    """

    def __init__(self, user, username, message):
        if username is None:
            # username = user for twitch anyway
            username = user

        self.User = user
        self.UserName = username
        self.Message = message
        self._params = string.split(self.Message)
        pass

    def GetParam(self, i):
        return self._params[i]

    def GetParamCount(self):
        return len(self._params)

    def IsChatMessage(self):
        return True

    def IsFromTwitch(self):
        return True


class User:
    def __init__(self, name, permissions, points=0):
        """
        name: str
        permissions: [str]
        """
        self.name = name
        self.permissions = permissions
        self.points = points
        pass


class Parent:
    """
    https://github.com/AnkhHeart/Streamlabs-Chatbot-Python-Boilerplate/wiki/Parent
    """

    def __init__(self, users=None):
        """
        users: { username: str -> User }
        """
        if users is None:
            users = {
                "pirak__": User("pirak__", ["Caster"], 1000),
                "uneven_points": User("uneven_points", ["caster"], 99),
                "a_mod": User("a_mod", ["Moderator"], 100),
                "someone": User("someone", ["Viewer"], 200),
            }

        self.users = users
        self.message_cache = []
        pass

    def GetPoints(self, user):
        """
        user: str
        """
        return self.users[user].points

    def AddPoints(self, user_id, username, amount):
        """
        user_id: str
        username: str
        amount: int
        """
        self.users[username].points += amount
        return True

    def RemovePoints(self, user_id, username, amount):
        """
        user_id: str
        username: str
        amount: int
        """
        if self.users[username].points < amount:
            return False
        else:
            self.users[username].points -= amount
            return True

    def AddPointsAllAsync(self, data, callback):
        """
        data: { username: str -> amount: int }
        callback: Function([str]) -> ()
        """
        failed_users = []
        for u in data.keys():
            if not self.AddPoints(u, u, data[u]):
                failed_users.append(u)
        callback(failed_users)

    def HasPermission(self, user, permission, unknown_param):
        """
        user: str
        permission: str
        original HasPermission has a third parameter `info`
        """
        if user not in self.users.keys():
            return False
        return permission in self.users[user].permissions

    def SendStreamMessage(self, message):
        """
        message: str

        Save message into message_cache
        """
        self.message_cache.append(message)

    def Log(self, script_name, message):
        print(message)
