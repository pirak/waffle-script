# -*- coding: utf-8 -*-

# The following License only applies to everything at line 121 or below. The
# settings class (lines 42-120) has been adapted from castorr91. The original
# copyright applies.

# Copyright (C) 2020 pirak, MonzUn
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

################################################################################

import codecs
import ctypes
import json
import math
import os
import re
import time
from threading import Timer

################################################################################

# Uncomment during development to silence Parent not found errors
# Has to be commented out for unit-testing and actually running the script
# from waffle.test_model import Parent
# Parent = Parent()

ScriptName = "Waffle"
Website = "https://www.twitch.tv/pirak__"
Description = "Lets users bet on the duration of waffling"
Creator = "pirak"
Version = "0.6.0"

################################################################################
# Load/Store settings from Json-File
#
# Settings class adapted from castorr91's (https://www.twitch.tv/castorr91)
# Gambling Script.
# Available in the AnkhBot discord https://discord.gg/Xv9dzD9

my_settingsfile = "settings_waffle.json"
my_settings = None
MB_YES = 6


class Settings:
    """" Loads settings from file if file is found if not uses default values"""

    # The 'default' variable names need to match UI_Config
    def __init__(self, settingsfile=None):
        if settingsfile and os.path.isfile(settingsfile):
            with codecs.open(settingsfile, encoding='utf-8-sig', mode='r') as f:
                self.__dict__ = json.load(f, encoding='utf-8-sig')
        else:  # set variables if no custom settings file is found
            self.command = "!waffle"
            self.min_gamble = 10
            self.entry_time = 15
            self.entry_reminder_before_end_enabled = True
            # how many minutes before the entry time ends the users should be
            # reminded about entering
            self.entry_reminder_before_end = 2
            self.mult_exact = 3
            self.msg_already_gambled = "{0}, you are already entered to bet this round!"
            self.msg_already_scheduled = "Waffling could not be scheduled as there is already one already scheduled to start in {0} minutes."
            self.msg_cancel_gamble = "Cancelled the current waffling and returned points!"
            self.msg_correct_usage = "Use !waffle <minutes> <bet_points> to bet."
            self.msg_currently_no_waffling = "Currently no waffling in progress!"
            self.msg_current_waffling_time = "Currently waffling for {0} minutes!"
            self.msg_entry_reminder = "Entries to bet on waffling duration are closing in {0} minutes!"
            self.msg_min_gamble_info = "You need to bet at least {0} points!"
            self.msg_no_longer_entry = "{0}, entries for betting have already closed!"
            self.msg_not_enough_points = "{0}, you do not have enough points to bet!"
            self.msg_no_win = "Waffling ended after {0} minutes! No user entered!"
            self.msg_scheduled_cancelled = "Scheduled waffling has been cancelled."
            self.msg_scheduled_waffling_time = "Waffling scheduled to start in {0} minutes!"
            self.msg_win_multiple = "Waffling ended after {2} minutes! Users {0} were closest with a prediction off by {1} min and won {3} points each!"
            self.msg_win_single = "Waffling ended after {2} minutes! User {0} was closest with a prediction off by {1} min and won {3} points!"

    # Reload settings on save through UI
    def Reload(self, data):
        """Reload settings on save through UI"""
        self.__dict__ = json.loads(data, encoding='utf-8-sig')

    def Save(self, settingsfile):
        """ Save settings contained within to .json and .js settings files. """
        try:
            with codecs.open(settingsfile, encoding="utf-8-sig", mode="w+") as f:
                json.dump(self.__dict__, f, encoding="utf-8", ensure_ascii=False)
            with codecs.open(settingsfile.replace("json", "js"), encoding="utf-8-sig", mode="w+") as f:
                f.write("var settings = {0};".format(json.dumps(self.__dict__, encoding='utf-8', ensure_ascii=False)))
        except ValueError:
            Parent.Log(ScriptName, "Failed to save settings to file.")


def SetDefaults():
    """Set default settings function"""
    MessageBox = ctypes.windll.user32.MessageBoxW
    returnValue = MessageBox(0, u"You are about to reset the settings, "
                                "are you sure you want to continue?"
                             , u"Reset settings file?", 4)
    if returnValue == MB_YES:
        returnValue = MessageBox(0, u"Settings successfully restored to default values"
                                 , u"Reset complete!", 0)
        global my_settings
        Settings.Save(my_settings, my_settingsfile)


def ReloadSettings(jsonData):
    """Reload settings on pressing the save button"""
    global my_settings
    my_settings.Reload(jsonData)


def SaveSettings():
    """Save settings on pressing the save button"""
    Settings.Save(my_settings, my_settingsfile)


################################################################################
################################################################################
################################################################################
# Actual implementation

class StateError(Exception):
    def __init__(self, message):
        """
        The message should be displayable to users in chat.

        message: str
        """
        self.message = message


class State:
    def __init__(self):
        self.waffle_jackpot = 0
        # maps minutes -> Users : { int -> [str] }
        self._current_bets = {}
        # maps User -> points entered : { str -> int }
        self._current_bet_amounts = {}
        self.start_time = None
        self.start_scheduled_time = None
        self._schedule_timer = None
        self._reminder_timer = None

    def reset(self):
        """
        Reset the state to no round running.

        Stops all timers and removes all made bets in the pot.
        Does not handle any returning of points.
        -> ()
        """
        self.waffle_jackpot = 0
        self._current_bets = {}
        self._current_bet_amounts = {}
        self.start_time = None
        self.start_scheduled_time = None
        if self._schedule_timer is not None:
            self._schedule_timer.cancel()
        self._schedule_timer = None
        if self._reminder_timer is not None:
            self._reminder_timer.cancel()
        self._reminder_timer = None

    def is_round_running(self):
        """
        -> bool
        """
        return self.start_time is not None

    def is_round_scheduled(self):
        """
        -> bool
        """
        return self.start_scheduled_time is not None

    def is_user_entered(self, user):
        """
        user: str
        -> bool
        """
        return any(map(lambda users_item: user in users_item[1],
                       self._current_bets.items()))

    def is_entering_closed(self):
        if not self.is_round_running():
            return True

        now = time.time()
        diff = minutes_from_seconds(now - self.start_time)
        return diff > my_settings.entry_time

    def start(self):
        """
        Hard-reset current round if present and start a new one.

        -> ()
        """
        self.reset()
        self.start_time = time.time()

        if my_settings.entry_reminder_before_end_enabled:
            remind_in = my_settings.entry_time - my_settings.entry_reminder_before_end
            if remind_in > 0:
                self._reminder_timer = Timer(remind_in * 60, send_entry_reminder)
                self._reminder_timer.start()

    def schedule_start(self, seconds):
        """
        Schedule the waffling to start in `seconds`.

        seconds: int
        -> ()
        """
        self._schedule_timer = Timer(seconds, execute_start)
        self._schedule_timer.start()
        self.start_scheduled_time = time.time() + seconds

    def add_bet(self, user_id, username, minutes, amount):
        """
        Enter a user into the bet.

        # Errors (StateError)
        - if minutes is less than or equal to 0
        - if amount is less than allowed minimum amount

        user_id: str
        username: str
        minutes: int
        amount: int
        -> ()
        """
        if minutes <= 0:
            raise StateError("Predicted minutes of waffling has to be > 0!")

        if amount < my_settings.min_gamble:
            raise StateError(my_settings.msg_min_gamble_info.format(my_settings.min_gamble))

        # make sure the user has enough points
        if amount > Parent.GetPoints(user_id):
            raise StateError(my_settings.msg_not_enough_points.format(username))

        Parent.RemovePoints(user_id, username, amount)

        self.waffle_jackpot += amount
        users = self._current_bets.get(minutes, [])
        users.append(username)
        self._current_bets[minutes] = users
        self._current_bet_amounts[username] = amount

    def find_closest(self, to):
        """
        Find the user(s) with bets closest to `to` minutes

        to: int; duration in minutes to search the closest to
        """
        closest = None

        for prediction, user_list in self._current_bets.items():
            diff = abs(prediction - to)
            if closest is None or diff < closest[0]:
                closest = (diff, user_list)
            # might happen for e.g. actual=2 and existing bets 1 and 3
            elif diff == closest[0]:
                closest[1].extend(user_list)

        return closest

    def return_points_to_users(self, callback_fail):
        """
        callback_fail: (users_failed: List[str]) -> ()
        - users_failed: usernames to which no points could be returned
        """
        Parent.AddPointsAllAsync(self._current_bet_amounts, callback_fail)


state = State()
re_percent = re.compile(r"^(100|\d{1,2})%$")

################################################################################
# Required functions


def Init():
    global my_settings
    my_settings = Settings(my_settingsfile)
    return


def Execute(data):
    if not data.IsChatMessage() or not data.IsFromTwitch():
        return

    if data.GetParam(0).lower() != my_settings.command:
        return

    param_count = data.GetParamCount()

    # the command itself counts as param
    if param_count == 1:
        handle_info(data)
    elif param_count > 1 and data.GetParam(1).lower() in ["start", "stop"]:
        handle_start_stop(data)
    elif param_count > 1 and data.GetParam(1).lower() == "cancel":
        handle_cancel(data)
    elif param_count == 3:
        handle_gamble(data)


def Tick():
    return


################################################################################
# Custom functions

def minutes_from_seconds(secs):
    return int(round(abs(secs) / 60.0, 0))


def minutes_from_seconds_ceil(secs):
    return int(math.ceil(abs(secs) / 60.0))


def user_points_percent(user, percent):
    """
    Returns the given percentage of total points of the user.

    Does not deduct any points from the user’s account.
    Expects percent as value in range [0, 100]
    """
    return int(Parent.GetPoints(user) * (percent / 100.0))


def has_mod_permission(user):
    return Parent.HasPermission(user, "Caster", "") \
        or Parent.HasPermission(user, "Moderator", "")


def parse_points(user, arg):
    """
    Parse arg into a concrete point value.

    Parseable args:
    - `all`: returns the total amount of points for this user
    - `x%`: with x in range [0,100] inclusive
      e.g. if a user has 1000 points, `parse_points(user, "5%")` returns 50
    - any string directly parseable as integer value
      Does not check for negative values or points below the minimum allowed
      betting amount.
    """
    arg = arg.lower().strip()
    if arg == "all":
        return Parent.GetPoints(user)
    elif re_percent.match(arg) is not None:
        percent = int(arg[:-1])
        return user_points_percent(user, percent)
    else:
        return int(arg)


def award_points_callback(users_failed):
    """
    Parent.AddPointsAll() can not award points to users no longer in chat.
    """
    if len(users_failed) > 0:
        Parent.SendStreamMessage("Could not give points to users {0} because they are currently not in chat.".format(', '.join(users_failed)))


def send_usage():
    Parent.SendStreamMessage(my_settings.msg_correct_usage)


def send_entry_reminder():
    Parent.SendStreamMessage(my_settings.msg_entry_reminder.format(my_settings.entry_reminder_before_end))
    send_usage()


def handle_info(data):
    """
    Show information about the current gambling round.
    """
    if not has_mod_permission(data.User):
        return

    if state.is_round_running():
        now = time.time()
        diff = minutes_from_seconds(now - state.start_time)
        Parent.SendStreamMessage(my_settings.msg_current_waffling_time.format(diff))
    elif state.is_round_scheduled():
        now = time.time()
        diff = minutes_from_seconds_ceil(state.start_scheduled_time - now)
        Parent.SendStreamMessage(my_settings.msg_scheduled_waffling_time.format(diff))


def handle_start_stop(data):
    """
    Schedule, start or stop a gambling round.
    """
    if not has_mod_permission(data.User):
        return

    param = data.GetParam(1).lower()
    if param == "start":
        handle_start(data)
    elif param == "stop":
        handle_stop(data)


def handle_start(data):
    """
    Handle the `!waffle start <in minutes>` chat command.

    Does not abort a possibly running or scheduled round.
    """
    if state.is_round_running():
        Parent.SendStreamMessage("Other waffling running currently! Cannot start!")
        return

    if data.GetParamCount() == 2:
        execute_start()
    elif data.GetParamCount() == 3:
        if state.is_round_scheduled():
            now = time.time()
            diff = minutes_from_seconds_ceil(state.start_scheduled_time - now)
            Parent.SendStreamMessage(my_settings.msg_already_scheduled.format(diff))
            return

        try:
            seconds = int(data.GetParam(2)) * 60
        except ValueError:
            return

        if seconds > 0:
            state.schedule_start(seconds)
            Parent.SendStreamMessage(my_settings.msg_scheduled_waffling_time.format(data.GetParam(2)))
        else:
            Parent.SendStreamMessage("Scheduled minutes until waffling has to be > 0!")


def execute_start():
    """
    Start a new gambling round. Overwrites an existing round if present.
    """
    state.start()
    Parent.SendStreamMessage("Waffling started! {}".format(my_settings.msg_correct_usage))


def handle_stop(data):
    """
    Stop the current gambling round and award points to winners.
    """
    if not state.is_round_running():
        Parent.SendStreamMessage("Cannot stop! Currently no waffling in progress!")
        return

    # `!command stop` OR `!command stop duration`
    if data.GetParamCount() == 2:
        stop_time = time.time()
        diff_minutes = minutes_from_seconds(stop_time - state.start_time)
    elif data.GetParamCount() == 3:
        try:
            diff_minutes = int(data.GetParam(2))
            if diff_minutes < 0:
                raise ValueError
        except ValueError:
            return
    else:
        return

    closest = state.find_closest(diff_minutes)
    if closest is None:
        Parent.SendStreamMessage(my_settings.msg_no_win.format(diff_minutes))
    else:
        handle_wins(closest[1], diff_minutes, closest[0])

    state.reset()


def handle_wins(users, actual_time, diff_actual):
    """
    Award points to winners.
    """
    points_per_user = int(round(state.waffle_jackpot / len(users), 0))
    if diff_actual == 0:
        points_per_user = points_per_user * my_settings.mult_exact

    points_map = {}
    for u in users:
        points_map[u] = points_per_user
    Parent.AddPointsAllAsync(points_map, award_points_callback)

    if len(users) == 1:
        message = my_settings.msg_win_single.format(users[0], diff_actual, actual_time, points_per_user)
    else:
        message = my_settings.msg_win_multiple.format(', '.join(users), diff_actual, actual_time, points_per_user)

    Parent.SendStreamMessage(message)


def handle_cancel(data):
    """
    Cancel a running betting round and return points.
    """
    if not has_mod_permission(data.User):
        return

    if state.is_round_scheduled():
        state.reset()
        Parent.SendStreamMessage(my_settings.msg_scheduled_cancelled)
        return

    if not state.is_round_running():
        Parent.SendStreamMessage("Cannot cancel! Currently no waffling in progress!")
        return

    state.return_points_to_users(award_points_callback)
    Parent.SendStreamMessage(my_settings.msg_cancel_gamble)
    state.reset()


def handle_gamble(data):
    """
    Handle users entering the gambling.
    """
    # make sure waffling is in progress
    if not state.is_round_running():
        Parent.SendStreamMessage(my_settings.msg_currently_no_waffling)
        return

    # make sure the user cannot enter twice
    if state.is_user_entered(data.User):
        Parent.SendStreamMessage(my_settings.msg_already_gambled.format(data.UserName))
        return

    # make sure the user cannot enter after the closing time
    if state.is_entering_closed():
        Parent.SendStreamMessage(my_settings.msg_no_longer_entry.format(data.UserName))
        return

    # all preconditions checked: now actual gamble entry handling is possible
    try:
        minutes = int(data.GetParam(1))
        gamble_amount = parse_points(data.User, data.GetParam(2))
        state.add_bet(data.User, data.UserName, minutes, gamble_amount)
    except ValueError:
        send_usage()
    except StateError as e:
        Parent.SendStreamMessage(e.message)
