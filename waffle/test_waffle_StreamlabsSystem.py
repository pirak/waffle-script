# -*- coding: utf-8 -*-

# Copyright (C) 2020 pirak
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

import datetime
import unittest

import pytest

import waffle_StreamlabsSystem
from waffle.test_model import *
from waffle_StreamlabsSystem import *


class TestScript(unittest.TestCase):
    def setUp(self):
        waffle_StreamlabsSystem.Parent = Parent()
        self.Parent = waffle_StreamlabsSystem.Parent
        self.state = waffle_StreamlabsSystem.state
        waffle_StreamlabsSystem.Init()

    def test_minutes_from_seconds(self):
        assert minutes_from_seconds(29) == 0
        assert minutes_from_seconds(30) == 1
        assert minutes_from_seconds(60) == 1
        assert minutes_from_seconds(60 + 29) == 1
        assert minutes_from_seconds(60 + 30) == 2
        assert minutes_from_seconds(7200 + 125) == 122

    def test_user_points_percent(self):
        assert user_points_percent("pirak__", 20) == 200
        assert user_points_percent("uneven_points", 20) == 19

    def test_has_mod_permission(self):
        assert not has_mod_permission("uneven_points")
        assert has_mod_permission("pirak__")
        assert has_mod_permission("a_mod")

    def test_parse_points(self):
        user = "pirak__"

        assert parse_points(user, "all") == 1000
        assert parse_points(user, "all") == self.Parent.GetPoints(user)

        for i in range(0, 101):
            assert parse_points(user, "{}%".format(i)) == i * 10

        with pytest.raises(ValueError):
            parse_points(user, "101%")

    def test_points_award_callback(self):
        award_points_callback(["pirak__", "anyone"])
        assert self.Parent.message_cache.pop() \
               == "Could not give points to users pirak__, anyone because " \
                  "they are currently not in chat."

    def test_state_add_bet(self):
        self.state.add_bet("pirak__", "pirak__", 20, 30)
        assert self.state.is_user_entered("pirak__")
        assert self.state.waffle_jackpot == 30
        assert self.state._current_bets[20] == ["pirak__"]
        assert self.state._current_bet_amounts["pirak__"] == 30

    def test_is_entering_closed(self):
        assert self.state.is_entering_closed() is True
        self.state.start_time = time.time() - (5 * 60)
        assert self.state.is_entering_closed() is False
        self.state.start_time = time.time() - (20 * 60)
        assert self.state.is_entering_closed() is True


class IntegrationTest(unittest.TestCase):
    def setUp(self):
        waffle_StreamlabsSystem.Parent = Parent()
        self.Parent = waffle_StreamlabsSystem.Parent
        self.Settings = waffle_StreamlabsSystem.Settings()
        self.state = waffle_StreamlabsSystem.state
        waffle_StreamlabsSystem.Init()

    def tearDown(self):
        self.Parent.message_cache = []
        self.state.reset()

    def _latest_msg(self):
        if len(self.Parent.message_cache) > 0:
            return self.Parent.message_cache.pop()
        else:
            return None

    def _start_time_move_by(self, minutes=15, seconds=0, backwards=True):
        delta = datetime.timedelta(minutes=minutes, seconds=seconds)
        if self.state.start_time is not None:
            sign = -1 if backwards else 1
            self.state.start_time += sign * delta.total_seconds()

    def _scheduled_start_time_move_by(self, minutes=15, seconds=0,
                                      backwards=True):
        delta = datetime.timedelta(minutes=minutes, seconds=seconds)
        if self.state.start_scheduled_time is not None:
            sign = -1 if backwards else 1
            self.state.start_scheduled_time \
                += sign * delta.total_seconds()

    def test_no_params(self):
        """
        Not-mods should not be able to show waffle info
        """
        Execute(Data("anyone", "anyone", "!waffle"))
        assert self._latest_msg() is None

    def test_start_waffle(self):
        """
        Only mods should be able to start waffle rounds
        """
        Execute(Data("anyone", "anyone", "!waffle start"))
        assert self.state.start_time is None
        assert self.state.start_scheduled_time is None
        assert self._latest_msg() is None

        Execute(Data("pirak__", "pirak__", "!waffle start"))
        assert self.state.start_time is not None
        assert self.state.start_scheduled_time is None
        assert self.state._reminder_timer is not None
        assert self._latest_msg() == "Waffling started! {}".format(
            self.Settings.msg_correct_usage)

    def test_start_waffle_twice(self):
        """
        A second start should not overwrite an already running one
        """
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        assert self.state.start_time is not None
        assert self.state.start_scheduled_time is None
        assert self._latest_msg() == "Waffling started! {}".format(
            self.Settings.msg_correct_usage)

        Execute(Data("pirak__", "pirak__", "!waffle start"))
        assert self.state.start_time is not None
        assert self.state.start_scheduled_time is None
        assert self._latest_msg() \
               == "Other waffling running currently! Cannot start!"

    def test_start_waffle_scheduled(self):
        Execute(Data("pirak__", "pirak__", "!waffle start 2x"))
        assert self._latest_msg() is None
        assert self.state.start_scheduled_time is None

        Execute(Data("pirak__", "pirak__", "!waffle start -1"))
        assert self._latest_msg() \
               == "Scheduled minutes until waffling has to be > 0!"
        assert self.state.start_scheduled_time is None

        Execute(Data("pirak__", "pirak__", "!waffle start 5"))
        assert self._latest_msg() \
               == self.Settings.msg_scheduled_waffling_time.format(5)
        # scheduled time is approx. 300 seconds in the future
        assert self.state.start_time is None
        assert abs(self.state.start_scheduled_time - (time.time() + 300)) < 1

    def test_start_waffle_scheduled_twice(self):
        """
        A second schedule should not overwrite the old one
        """
        Execute(Data("pirak__", "pirak__", "!waffle start 5"))
        assert self.state.start_scheduled_time is not None
        assert self.state._schedule_timer is not None
        Execute(Data("pirak__", "pirak__", "!waffle start 3"))
        assert self._latest_msg() \
               == self.Settings.msg_already_scheduled.format(5)
        assert abs(self.state.start_scheduled_time
                   - (time.time() + 300)) < 1

    def test_waffle_info(self):
        """
        Should only send something if round is running and caller has permissions
        """
        Execute(Data("pirak__", "pirak__", "!waffle"))
        assert self._latest_msg() is None

        Execute(Data("pirak__", "pirak__", "!waffle start"))
        self.Parent.message_cache.pop()

        Execute(Data("pirak__", "pirak__", "!waffle"))
        assert self._latest_msg() \
               == self.Settings.msg_current_waffling_time.format(0)

        Execute(Data("a_mod", "a_mod", "!waffle"))
        assert self._latest_msg() \
               == self.Settings.msg_current_waffling_time.format(0)

        Execute(Data("anyone", "anyone", "!waffle"))
        assert self._latest_msg() is None

        self._start_time_move_by(minutes=10, seconds=20)
        Execute(Data("a_mod", "a_mod", "!waffle"))
        assert self._latest_msg() \
               == self.Settings.msg_current_waffling_time.format(10)

    def test_waffle_info_scheduled(self):
        """
        !waffle should show the correct information if a waffling is scheduled
        """
        Execute(Data("pirak__", "pirak__", "!waffle start 5"))
        assert self.state.start_scheduled_time is not None
        assert self.state._schedule_timer is not None

        Execute(Data("pirak__", "pirak__", "!waffle"))
        assert self._latest_msg() \
               == self.Settings.msg_scheduled_waffling_time.format(5)

        self._scheduled_start_time_move_by(3)
        Execute(Data("pirak__", "pirak__", "!waffle"))
        assert self._latest_msg() \
               == self.Settings.msg_scheduled_waffling_time.format(2)

    def test_enter_gamble_success(self):
        """
        Various correct ways of entering the gambling
        """
        user = "a_mod"
        Execute(Data(user, user, "!waffle start"))
        Execute(Data(user, user, "!waffle 45 60"))
        assert self.state._current_bet_amounts[user] == 60
        assert self.Parent.GetPoints(user) == 40

        user = "pirak__"
        Execute(Data(user, user, "!waffle 20 all"))
        assert self.state._current_bet_amounts[user] == 1000
        assert self.Parent.GetPoints(user) == 0

        user = "uneven_points"
        Execute(Data(user, user, "!waffle 25 50%"))
        assert self.state._current_bet_amounts[user] == 49
        assert self.Parent.GetPoints(user) == 50

    def test_enter_gamble_fail(self):
        """
        Various incorrect ways of entering the gambling
        """
        user = "a_mod"
        Execute(Data(user, user, "!waffle start"))

        Execute(Data(user, user, "!waffle 45 101"))
        assert user not in self.state._current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() \
               == self.Settings.msg_not_enough_points.format(user)

        Execute(Data(user, user, "!waffle 45 0"))
        assert user not in self.state._current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() \
               == self.Settings.msg_min_gamble_info.format(self.Settings.min_gamble)

        Execute(Data(user, user, "!waffle 20 -1"))
        assert user not in self.state._current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() \
               == self.Settings.msg_min_gamble_info.format(self.Settings.min_gamble)

        Execute(Data(user, user, "!waffle 20 allX"))
        assert user not in self.state._current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() == self.Settings.msg_correct_usage

        Execute(Data(user, user, "!waffle 25 101%"))
        assert user not in self.state._current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() == self.Settings.msg_correct_usage

        Execute(Data(user, user, "!waffle 25 -3%"))
        assert user not in self.state._current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() == self.Settings.msg_correct_usage

        Execute(Data(user, user, "!waffle -3 10%"))
        assert user not in self.state._current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() \
               == "Predicted minutes of waffling has to be > 0!"

        Execute(Data(user, user, "!waffle 0 10%"))
        assert user not in self.state._current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() \
               == "Predicted minutes of waffling has to be > 0!"

    def test_gamble_enter_twice(self):
        """
        Should only enter once and not overwrite old entry
        """
        user = "pirak__"
        Execute(Data(user, user, "!waffle start"))
        Execute(Data(user, user, "!waffle 45 60"))
        assert self.state._current_bet_amounts[user] == 60
        assert self.Parent.GetPoints(user) == 940

        Execute(Data(user, user, "!waffle 45 120"))
        assert self.state._current_bet_amounts[user] == 60
        assert self.Parent.GetPoints(user) == 940
        assert self._latest_msg() \
               == self.Settings.msg_already_gambled.format(user)

    def test_gamble_not_started(self):
        """
        Should fail to enter
        """
        user = "a_mod"
        Execute(Data(user, user, "!waffle 45 60"))
        assert self.state.start_time is None
        assert user not in self.state._current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() == self.Settings.msg_currently_no_waffling

    def test_bet_additional_params(self):
        """
        Should be ignored completely
        """
        user = "a_mod"
        Execute(Data(user, user, "!waffle start"))

        Execute(Data(user, user, "!waffle 45 60 50"))
        assert user not in self.state._current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert len(self.Parent.message_cache) == 1

        Execute(Data(user, user, "!waffle 45 err 50"))
        assert user not in self.state._current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert len(self.Parent.message_cache) == 1

    def test_cancel_invalid(self):
        """
        Should only be possibly by mod and if gambling has started
        """
        # no permission
        Execute(Data("anyone", "anyone", "!waffle cancel"))
        assert self._latest_msg() is None

        # not started
        Execute(Data("pirak__", "pirak__", "!waffle cancel"))
        assert self._latest_msg() \
               == "Cannot cancel! Currently no waffling in progress!"

    def test_cancel_valid(self):
        """
        Should refund points to users
        """
        Execute(Data("pirak__", "pirak__", "!waffle start"))

        # still no permission
        Execute(Data("anyone", "anyone", "!waffle cancel"))
        assert self._latest_msg() \
               == "Waffling started! {}".format(self.Settings.msg_correct_usage)

        # let some users enter
        Execute(Data("pirak__", "pirak__", "!waffle 20 60"))
        assert self.Parent.GetPoints("pirak__") == 940
        Execute(Data("a_mod", "a_mod", "!waffle 43 20"))

        Execute(Data("pirak__", "pirak__", "!waffle cancel"))

        # check refunds
        assert self.Parent.GetPoints("pirak__") == 1000
        assert self.Parent.GetPoints("a_mod") == 100
        # check no longer running
        assert self.state.start_time is None
        assert self.state.start_scheduled_time is None

    def test_cancel_scheduled(self):
        """
        !waffle cancel should cancel scheduled starts
        """
        Execute(Data("pirak__", "pirak__", "!waffle start 2"))
        assert self.state.start_scheduled_time is not None
        assert self.state._schedule_timer is not None
        Execute(Data("pirak__", "pirak__", "!waffle cancel"))
        assert self._latest_msg() == "Scheduled waffling has been cancelled."
        assert self.state.start_time is None
        assert self.state.start_scheduled_time is None
        assert self.state._schedule_timer is None

    def test_enter_after_closing(self):
        """
        Entering after closing should not be possible
        """
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        self._start_time_move_by(minutes=self.Settings.entry_time + 2)
        Execute(Data("a_mod", "a_mod", "!waffle 20 40"))
        assert self._latest_msg() \
               == self.Settings.msg_no_longer_entry.format("a_mod")

    def test_stop_not_running(self):
        """
        Stop should not be possible if no round is running
        """
        Execute(Data("pirak__", "pirak__", "!waffle stop"))
        assert self._latest_msg() \
               == "Cannot stop! Currently no waffling in progress!"

    def test_stop_no_enter_no_duration(self):
        """
        Message displays and state resets properly even after nobody entered
        """
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        self._start_time_move_by()
        Execute(Data("pirak__", "pirak__", "!waffle stop"))
        assert self._latest_msg() == self.Settings.msg_no_win.format(15)
        assert self.state.start_time is None

    def test_stop_no_enter_with_duration(self):
        """
        Should behave the same as test_stop_no_enter_no_duration
        """
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        self._start_time_move_by(minutes=15)
        Execute(Data("pirak__", "pirak__", "!waffle stop 55"))
        assert self._latest_msg() == self.Settings.msg_no_win.format(55)
        assert self.state.start_time is None

    def test_stop_no_enter_with_invalid_duration(self):
        """
        On invalid durations on waffle stop the command should be ignored
        """
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        self._start_time_move_by(minutes=15)
        # should be ignored, waffling still running
        Execute(Data("pirak__", "pirak__", "!waffle stop -2"))
        assert len(self.Parent.message_cache) == 1
        Execute(Data("pirak__", "pirak__", "!waffle stop 5a"))
        assert len(self.Parent.message_cache) == 1
        assert self.state.start_time is not None

    def test_win_single(self):
        """
        The message for a single winner should be displayed
        """
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        Execute(Data("pirak__", "pirak__", "!waffle 40 20"))
        Execute(Data("a_mod", "a_mod", "!waffle 45 60"))
        self._start_time_move_by(minutes=46)
        Execute(Data("pirak__", "pirak__", "!waffle stop"))
        assert self._latest_msg() \
               == self.Settings.msg_win_single.format("a_mod", 1, 46, 80)
        assert self.state.start_time is None

    def test_win_single_exact(self):
        """
        The jackpot should be multiplied for a lone winner with exact prediction
        """
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        Execute(Data("pirak__", "pirak__", "!waffle 46 20"))
        Execute(Data("a_mod", "a_mod", "!waffle 40 60"))
        self._start_time_move_by(minutes=46)
        Execute(Data("pirak__", "pirak__", "!waffle stop"))
        m = self.Settings.mult_exact
        assert self._latest_msg() \
               == self.Settings.msg_win_single.format("pirak__", 0, 46, 80 * m)
        assert self.state.start_time is None

    def test_win_multiple(self):
        """
        The message for multiple winners should be displayed
        """
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        Execute(Data("uneven_points", "uneven_points", "!waffle 48 60"))
        Execute(Data("pirak__", "pirak__", "!waffle 45 20"))
        Execute(Data("a_mod", "a_mod", "!waffle 47 60"))
        Execute(Data("someone", "someone", "!waffle 50 20"))
        self._start_time_move_by(minutes=46)
        Execute(Data("pirak__", "pirak__", "!waffle stop"))
        winners = "pirak__, a_mod"
        assert self._latest_msg() \
               == self.Settings.msg_win_multiple.format(winners, 1, 46, 80)
        assert self.state.start_time is None

    def test_win_multiple_exact(self):
        """
        The jackpot should be multiplied for multiple winners with exact
        predictions
        """
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        Execute(Data("pirak__", "pirak__", "!waffle 46 20"))
        Execute(Data("a_mod", "a_mod", "!waffle 46 60"))
        self._start_time_move_by(minutes=46)
        Execute(Data("pirak__", "pirak__", "!waffle stop"))
        winners = "pirak__, a_mod"
        m = self.Settings.mult_exact
        assert self._latest_msg() \
               == self.Settings.msg_win_multiple.format(winners, 0, 46, 40 * m)
        assert self.state.start_time is None

    def test_handle_stop_too_many_args(self):
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        data = Data("pirak__", "pirak__", "!waffle stop 23 8 123")
        assert handle_stop(data) is None

    def test_handle_start_too_many_args(self):
        data = Data("pirak__", "pirak__", "!waffle start 23 8 123")
        assert handle_start(data) is None
        assert self.state.start_time is None
        assert self.state.is_round_running() is False

    def test_entry_reminder(self):
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        assert self.state.start_time is not None
        assert self.state._reminder_timer is not None

    def test_entry_reminder_scheduled(self):
        Execute(Data("pirak__", "pirak__", "!waffle start 5"))
        assert self.state._reminder_timer is None

    def test_entry_reminder_disabled(self):
        waffle_StreamlabsSystem.my_settings.entry_reminder_before_end_enabled = False
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        assert self.state.start_time is not None
        assert self.state._reminder_timer is None

    def test_entry_reminder_too_long(self):
        waffle_StreamlabsSystem.my_settings.entry_reminder_before_end = 15
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        assert self.state.start_time is not None
        assert self.state._reminder_timer is None

    def test_send_entry_reminder(self):
        send_entry_reminder()
        assert self._latest_msg() == self.Settings.msg_correct_usage
        t = self.Settings.entry_reminder_before_end
        assert self._latest_msg() == self.Settings.msg_entry_reminder.format(t)

    def test_handle_start_stop_invalid(self):
        """
        Should not print anything and not change state.

        `handle_start_stop()` should only be called for commands
        `!waffle start …` or
        `!waffle stop …` in actual usage.
        """
        data = Data("pirak__", "pirak__", "!waffle bogus extra")
        handle_start_stop(data)
        assert self._latest_msg() is None
        assert self.state.is_round_running() is False

    def test_execute_not_command(self):
        """
        Ignore messages not starting with !waffle
        """
        Execute(Data("pirak__", "pirak__", "waffle random message ignored"))
        assert self._latest_msg() is None

    def test_execute_not_chat_message(self):
        data = Data("pirak__", "pirak__", "!waffle start")
        data.IsChatMessage = lambda : False
        Execute(data)
        assert self._latest_msg() is None
        assert self.state.is_round_running() is False

    def test_tick(self):
        """
        Tick is not used, should do nothing
        """
        waffle_StreamlabsSystem.Tick()
        assert self._latest_msg() is None


def test_save_settings():
    """
    Save and load of settings
    """
    waffle_StreamlabsSystem.SaveSettings()
    waffle_StreamlabsSystem.my_settings = waffle_StreamlabsSystem.Settings(
        waffle_StreamlabsSystem.my_settingsfile)

    assert os.path.exists(waffle_StreamlabsSystem.my_settingsfile)
    os.remove(waffle_StreamlabsSystem.my_settingsfile)
    f2 = waffle_StreamlabsSystem.my_settingsfile.replace(".json", ".js")
    assert os.path.exists(f2)
    os.remove(f2)


def test_save_reload_settings():
    waffle_StreamlabsSystem.SaveSettings()
    assert os.path.exists(waffle_StreamlabsSystem.my_settingsfile)
    with codecs.open(waffle_StreamlabsSystem.my_settingsfile, mode="r", encoding="utf-8-sig") as f:
        js = f.read()
        js = js.replace("\"min_gamble\": 10", "\"min_gamble\": 500")
        waffle_StreamlabsSystem.ReloadSettings(js)
        assert waffle_StreamlabsSystem.my_settings.min_gamble == 500

    os.remove(waffle_StreamlabsSystem.my_settingsfile)
    os.remove(waffle_StreamlabsSystem.my_settingsfile.replace(".json", ".js"))
