# Changelog

## Unreleased Changes
- …

## v0.6.0 (2020-11-26)
- [add] option to send a reminder with a configurable time before the entries are closed (!7)

## v0.5.0 (2020-11-16)
- [add] start command variation: `!waffle start <delay>` (!5, MonzUn)
- [fix] `!waffle stop` command not stopping in progress waffle if no bets were made (!5, MonzUn)

## v0.4.3 (2020-11-13):
- [fix] usage message default
- [fix] manual duration on stop could be negative
- [fix] missing spaces between multiple wining users
- [fix] gambling on zero minutes was possible
- [fix] properly parse single digit percentages
- [fix] format failure message of award points callback

## v0.4.2 (2020-11-08):
- [fix] AddPoints prints error message even if no failure occurred.

## v0.4.1 (2020-11-07):
- [fix] wrong index in `stop` with duration.

## v0.4.0 (2020-11-07):
- [add] stop command variation: `!waffle stop <duration>` to fix the waffling
  duration to the given length (e.g. after the stop was forgotten).
- [add] cancel command: `!waffle cancel` to stop an in-progress waffling.
  Returns entered points to users.
- [fix] `start` forcefully overwrites old potentially still running gambling
  round.

## v0.3.0 (2020-11-07):
- [add] let users enter percentage or `all` for their bet entry.

## v0.2.0
- [add] setting to enable a time after which no more entries are possible.

## v0.1.0
- Initial (fairly untested) version.
